\babel@toc {english}{}\relax 
\contentsline {chapter}{\numberline {1}State of the Art}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Computer Vision tasks}{3}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}Object Detection}{3}{subsection.1.1.1}%
\contentsline {subsection}{\numberline {1.1.2}Instance Segmentation}{4}{subsection.1.1.2}%
\contentsline {subsubsection}{ResNet}{4}{section*.6}%
\contentsline {subsubsection}{ResNet-50}{5}{section*.8}%
\contentsline {subsubsection}{ResNext}{6}{section*.10}%
\contentsline {subsubsection}{PointRend}{7}{section*.13}%
\contentsline {subsection}{\numberline {1.1.3}Semantic Segmentation}{9}{subsection.1.1.3}%
\contentsline {subsubsection}{FCN}{9}{section*.16}%
\contentsline {subsubsection}{U-Net}{10}{section*.17}%
\contentsline {subsubsection}{DeepLabv3}{11}{section*.19}%
\contentsline {subsection}{\numberline {1.1.4}Panoptic Segmentation}{12}{subsection.1.1.4}%
\contentsline {section}{\numberline {1.2}Detectors}{12}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Two-stage Detectors}{12}{subsection.1.2.1}%
\contentsline {subsubsection}{R-CNN}{12}{section*.21}%
\contentsline {subsubsection}{Fast R-CNN}{13}{section*.22}%
\contentsline {subsubsection}{Faster R-CNN}{13}{section*.23}%
\contentsline {subsubsection}{Mask R-CNN}{14}{section*.24}%
\contentsline {subsection}{\numberline {1.2.2}One-stage Detectors}{14}{subsection.1.2.2}%
\contentsline {subsubsection}{Feature Pyramid Network (FPN)}{15}{section*.27}%
\contentsline {section}{\numberline {1.3}Loss Metrics}{16}{section.1.3}%
\contentsline {chapter}{\numberline {2}Data and Methodology}{18}{chapter.2}%
\contentsline {section}{\numberline {2.1}Dataset}{18}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Original Dataset}{18}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Data Augmentation}{21}{subsection.2.1.2}%
\contentsline {subsection}{\numberline {2.1.3}Edited Class Dataset}{22}{subsection.2.1.3}%
\contentsline {subsection}{\numberline {2.1.4}Self Annotated Dataset}{22}{subsection.2.1.4}%
\contentsline {subsection}{\numberline {2.1.5}Combined Dataset}{24}{subsection.2.1.5}%
\contentsline {section}{\numberline {2.2}Early Trials}{25}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Deeplab with Pytorch}{25}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Detectron2}{27}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Detectron2 Loss Metrics}{27}{subsection.2.2.3}%
\contentsline {section}{\numberline {2.3}Hyperparameter Optimization}{28}{section.2.3}%
\contentsline {chapter}{\numberline {3}Results and Discussion}{31}{chapter.3}%
\contentsline {section}{\numberline {3.1}R50, FPN, 3x}{31}{section.3.1}%
\contentsline {section}{\numberline {3.2}X101, FPN, 3x}{32}{section.3.2}%
\contentsline {section}{\numberline {3.3}X101 with PointRend}{36}{section.3.3}%
\contentsline {section}{\numberline {3.4}Final Training}{38}{section.3.4}%
\contentsline {subsection}{\numberline {3.4.1}X101, New Dataset}{38}{subsection.3.4.1}%
\contentsline {subsection}{\numberline {3.4.2}X101, Combined Dataset}{43}{subsection.3.4.2}%
\contentsline {subsection}{\numberline {3.4.3}R50, New Dataset}{48}{subsection.3.4.3}%
\contentsline {subsection}{\numberline {3.4.4}R50, Combined Dataset}{53}{subsection.3.4.4}%
